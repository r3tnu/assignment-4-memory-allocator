#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query && block->is_free; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );

static struct block_header *heap_last(struct block_header *head) {
    if (!head) return NULL;
    while (head->next) head = head->next;
    return head;
}

static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/* Allocate map memory and form a region struct as a return*/
static struct region alloc_region  ( void const * addr, size_t query ) {
  size_t actual_region_size = region_actual_size(query + offsetof(struct block_header, contents));

  void* region_ptr = map_pages(addr, actual_region_size, MAP_FIXED);
  if (region_ptr == MAP_FAILED) region_ptr = map_pages(addr, actual_region_size, 0);
  if (region_ptr == MAP_FAILED) return REGION_INVALID;

  block_init(region_ptr, (block_size) {actual_region_size}, NULL);
  return (struct region) { .addr = region_ptr, .size = actual_region_size, .extends = (addr == region_ptr) };
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;
  return region.addr;
}

static bool blocks_continuous (struct block_header const* fst,
                               struct block_header const* snd );


/* Free all memory allocated for the heap*/
void heap_term( ) {
  void* current_region_address = HEAP_START;
  struct block_header* current_block = current_region_address;
  size_t current_region_size = 0;
  while (current_block) {
    struct block_header* next_block = current_block->next;
    current_region_size += size_from_capacity(current_block->capacity).bytes;
    if (next_block && !blocks_continuous(current_block, next_block)) {
      munmap(current_region_address, current_region_size);
      current_region_address = next_block;
      current_region_size = 0;
    } else if (!next_block) {
      munmap(current_region_address, current_region_size);
      break;
    }
    current_block = next_block;
  }
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block->is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  if (!block) return false;
  size_t actual_query = size_max(query, BLOCK_MIN_CAPACITY);
  if (!block_splittable(block, actual_query)) return false;

  block_capacity new_block_capacity = (block_capacity) { block->capacity.bytes - actual_query - offsetof(struct block_header, contents) };

  block->capacity.bytes = actual_query;

  block_init(block_after(block), size_from_capacity(new_block_capacity), block->next);

  block->next = block_after(block);

  return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  if( !block->next || !block) return false;

  struct block_header* next_block = block->next;
  if( !mergeable(block, next_block) ) return false;

  block->next = next_block->next;
  block->capacity.bytes += next_block->capacity.bytes + offsetof(struct block_header, contents);
  return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
  if (!block) return (struct block_search_result) { .type = BSR_CORRUPTED };
  struct block_search_result search_result = { .type = BSR_REACHED_END_NOT_FOUND, .block = NULL };
  while (true) {
    if (block->is_free) {
        while (try_merge_with_next(block));
    }

    if (!search_result.block && block_is_big_enough(sz, block)) {
      search_result.block = block;
      search_result.type = BSR_FOUND_GOOD_BLOCK;
    }
    if (!block->next) break;
    block = block->next;
  }

  if (!search_result.block) search_result.block = block;

  return search_result;
}

/* Try to allocate memory in the heap from the block `block` while not extending the heap*/
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  if (query < BLOCK_MIN_CAPACITY) query = BLOCK_MIN_CAPACITY;
  struct block_search_result search_result = find_good_or_last(block, query);
  if (search_result.type != BSR_FOUND_GOOD_BLOCK) return search_result;

  split_if_too_big(search_result.block, query);
  search_result.block->is_free = false;
  return search_result;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  if (!last) return NULL;

  void* new_region_desired_location = block_after(last);
  struct region next_region = alloc_region(new_region_desired_location, query);
  if (region_is_invalid(&next_region)) return NULL;

  last->next = next_region.addr;

  if (next_region.extends && try_merge_with_next(last)) return last;
  return next_region.addr;
}

/* Main malloc logic
 Return block_header of the allocated block*/
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  if (query <= 0 || !heap_start) return NULL;
  struct block_search_result search_result = try_memalloc_existing(query, heap_start);
  switch (search_result.type) {
    case BSR_CORRUPTED: return NULL; break;
    case BSR_FOUND_GOOD_BLOCK: return search_result.block; break;
    case BSR_REACHED_END_NOT_FOUND: grow_heap(heap_last(heap_start), query);
  }

  search_result = try_memalloc_existing(query, heap_start);
  if (search_result.type == BSR_FOUND_GOOD_BLOCK) return search_result.block;
  return NULL;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return;
  struct block_header* header = block_get_header( mem );
  if (!header) return;
  if (header->is_free) return;
  header->is_free = true;
  while(try_merge_with_next(header));
}
