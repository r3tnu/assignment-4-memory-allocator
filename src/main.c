#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <assert.h>
#define HEAP_INIT_CAPACITY 4096


struct block_header* get_header(void* addr){
    return (struct block_header*) (addr - offsetof(struct block_header, contents));
}

static void print_test_by_number(uint64_t n){
    printf("TEST %" PRIu64 ": ", n);
}

static void test_heap_init()
{
    size_t init_heap_size = HEAP_INIT_CAPACITY;
    void *result = heap_init(init_heap_size);
    assert(result != NULL && "TEST 1: HEAP INIT ERROR");
    print_test_by_number(1);
    debug_heap(stdout, HEAP_START);
    printf("\tSUCCESSFUL HEAP INITIALIZATION\n");
}

static void test_malloc_free()
{
    size_t block_size = 256;
    void* block = _malloc(block_size);
    assert(block != NULL && "TEST 2: BLOCK ALLOCATION ERROR");
    print_test_by_number(2);
    debug_heap(stdout, HEAP_START);
    printf("\tSUCCESSFUL BLOCK ALLOCATION\n");
    printf("\t*********\n");
    _free(block);
    
    assert(get_header(block)->is_free && "TEST 2: BLOCK WASN'T FREEING");
    printf("\tSUCCESSFUL BLOCK FREEING\n");
    debug_heap(stdout, HEAP_START);
}

static void test_exceeding_heap_block_init(){
    size_t block_size = HEAP_INIT_CAPACITY * 2;
    void *header = _malloc(block_size);
    assert(header != NULL && "TEST 3: INITIALISATION OF A BLOCK EXCEEDING THE HEAP CAPACITY ERROR");
    print_test_by_number(3);
    debug_heap(stdout, HEAP_START);

    printf("\tSUCCESSFUL INITIALISATION OF A BLOCK EXCEEDING THE HEAP CAPACITY\n");
}


int main() {
    test_heap_init();
    test_malloc_free();
    test_exceeding_heap_block_init();
    return 0;
}

